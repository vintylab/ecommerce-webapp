import { Observable, Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.css']
})
export class MyOrdersComponent implements OnInit, OnDestroy {
orders :any; 
  sub: Subscription = new Subscription;
  constructor(private orderService: OrderService) { }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  ngOnInit(): void {
this.sub=this.orderService.getMy().subscribe( ite =>{
this.orders=ite; 
}
);

  }

  myfn(){
    if   (typeof this.orders !== 'undefined'   )
    return true;
    return false;
    }
  
  

}
