import { Observable } from 'rxjs';
import { Product } from './models/Product';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
private url_getall="http://localhost:8080/vintysite/products/getAll";
private url_create="http://localhost:8080/vintysite/products/addProduct";
private url_updat="http://localhost:8080/vintysite/products/update/";
private url_delete="http://localhost:8080/vintysite/products/delete/";
private url_get="http://localhost:8080/vintysite/products/getById/";
private hdrs=new HttpHeaders({
  'Content-Type': 'application/json',
  'response-Type':'text'
});
  constructor( private http: HttpClient) { }



  create(product: any){
 let options = {
    headers: this.hdrs
 }
return  this.http.post(this.url_create , JSON.stringify(product),options);

  }

  getAll(){
return this.http.get(this.url_getall);
           }

update(id: string, product:any){
  
 let options = {
    headers: this.hdrs
 }
 return this.http.put(this.url_updat + id,JSON.stringify(product),options);
 
}

getById (id: string): Observable<any> {
 return this.http.get(this.url_get+id)  ;
}
delete(id: any ){
  
 let options = {
    headers: this.hdrs
 }
return this.http.delete(this.url_delete+id,options);
}

}
