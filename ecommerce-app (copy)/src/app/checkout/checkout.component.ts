import { Router } from '@angular/router';
import { OrderService } from './../order.service';
import { Order } from './../models/Order';
import { ProductService } from './../product.service';
import { ShoppingCartService } from './../shopping-cart.service';
import { Component, OnInit } from '@angular/core';
import { Product } from '../models/Product';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

cart:any;
  order: Order ={ userId:"",timeOrdered:"",totalPrice:"", address:"" };
  products: Product[] = [];
  constructor(private orderService: OrderService,
    private router:Router,
    private cartService:ShoppingCartService ,
     private productService:ProductService 
  ) { }

  ngOnInit(): void {
    this.cartService.getCart(localStorage.getItem("userId")!).subscribe( obj => {this.cart=obj;
      this.loadProducts();
      
    } );

  }

  placeOrder(shipping:any) {
   let str=Date().toString();
   this.order.timeOrdered= str;
   this.order.address=shipping.name + ' ' +shipping.address + ' '+ shipping.city;
   this.order.totalPrice=this.calculateTotal().toString();
   this.order.userId=localStorage.getItem("userId")!;
   this.orderService.placeOrder(this.order)
   .subscribe( mess =>{ console.log(mess);
    this.cartService.clearCart(localStorage.getItem("userId")).subscribe(ob => console.log(ob));
  } );
    

   this.router.navigate(['/order-success']);



  }   

  private isloggedin(){
    if(localStorage.getItem("userId"))
    return true;
    return false;
   }

  getQuantity(){
    if(!this.isloggedin())
    return 0;
    let q=0;
    if(this.cart)
    this.cart.forEach((element: any) => {
      q+=element.quantity;
    });
  return q;
  }


  loadProducts(){
    if(this.cart)
    this.cart.forEach((element:any) => {
     this.productService.getById(element.productId.toString()).subscribe(obj => this.products.push(obj));

    });
    
  }

  myfn(){
  if   (typeof this.products !== 'undefined' && typeof this.cart !== 'undefined'  )
  return true;
  return false;
  }

calculateTotal(){
let i=0;
let total=0;
if(this.myfn())
  this.products.forEach( (element) => { 
  total+=this.cart[i].quantity * element.price;
   i=i+1;
   });
   return total;
}

}
