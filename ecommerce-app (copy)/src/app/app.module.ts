import { OrderService } from './order.service';
import { ShoppingCartService } from './shopping-cart.service';
import { ProductService } from './product.service';
import { AdminAuthGuardService } from './admin-auth-guard.service';
import { UserService } from './user.service';
import { AuthGuardService } from './auth-guard.service';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFireAuthModule} from '@angular/fire/auth';
import { BsNavbarComponent } from './bs-navbar/bs-navbar.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { OrderSuccessComponent } from './order-success/order-success.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { AdminProductsComponent } from './admin/admin-products/admin-products.component';
import { AdminOrdersComponent } from './admin/admin-orders/admin-orders.component';
import { LoginComponent } from './login/login.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from './auth.service';
import { ProductFormComponent } from './product-form/product-form.component';
import { FormsModule } from '@angular/forms';
import { ProductFormUpdateComponent } from './product-form-update/product-form-update.component';
import { HttpClientModule } from '@angular/common/http';
import { SignupComponent } from './signup/signup.component';

const config={
  apiKey: "AIzaSyAb4Vbyc3nyYZahqp0vPD4sfY7Ox5aIgb0",
  authDomain: "myfirebaseproject-1aa1f.firebaseapp.com",
  databaseURL: "https://myfirebaseproject-1aa1f-default-rtdb.firebaseio.com",
  projectId: "myfirebaseproject-1aa1f",
  storageBucket: "myfirebaseproject-1aa1f.appspot.com",
  messagingSenderId: "247791877805",
  appId: "1:247791877805:web:4fde8b7623d70991361024"

};

@NgModule({
  declarations: [
    AppComponent,
    ShoppingCartComponent,
    BsNavbarComponent,
    HomeComponent,
    ProductsComponent,
    CheckoutComponent,
    OrderSuccessComponent,
    MyOrdersComponent,
    AdminProductsComponent,
    AdminOrdersComponent,
    LoginComponent,
    ProductFormComponent,
    ProductFormUpdateComponent,
    SignupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path:'',component:HomeComponent},
      {path:'product/:productId',component:ProductsComponent},
      {path:'shopping-cart',component:ShoppingCartComponent},
      {path:'login',component:LoginComponent},
      {path:'signup',component:SignupComponent},
       
      
      {path:'checkout',component:CheckoutComponent, canActivate:[AuthGuardService]},
      {path:'order-success',component:OrderSuccessComponent,  canActivate:[AuthGuardService]},
      {path:'my/orders', component:MyOrdersComponent,  canActivate:[AuthGuardService]},

      
      {path:'admin/products/new',component:ProductFormComponent},
      {path:'admin/orders',component:AdminOrdersComponent,  canActivate:[AuthGuardService]},
      {path:'admin/products/:id',component:ProductFormUpdateComponent},
      {path:'admin/products',component:AdminProductsComponent }
    ]),
    NgbModule
  ],
  providers: [
    AuthService,
    AuthGuardService,
    UserService,
    AdminAuthGuardService,
    ProductService,
    ShoppingCartService,
    OrderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
