import { Observable, Subscription } from 'rxjs';
import { ProductService } from './../../product.service';
import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.css']
})
export class AdminProductsComponent implements OnInit,OnDestroy {
  products: any;
  filterProducts :any;
  subscription: Subscription = new Subscription;

  str: string[] = [];
  constructor(private productService:ProductService) { 
//this.subscription=productService.getAll().subscribe(products => this.filterProducts = this.products=products) ;
  }


  ngOnInit(): void {
   this.subscription= this.productService.getAll()
    .subscribe(
      (      response: any)  => {
       this.filterProducts= this.products=response;
      },
      (      error: any)=>{ 
         alert('An unexpected error has occured');
         console.log(error);
       });
  }
ngOnDestroy(){

  this.subscription.unsubscribe();
}


filter(srch:string){
this.filterProducts= (srch)?
this.products.filter((p: { title: string; }) => p.title.toLowerCase().includes(srch.toLowerCase())):
this.products;
}

}
