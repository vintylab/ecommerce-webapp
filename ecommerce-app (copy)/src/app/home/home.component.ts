import { ActivatedRoute } from '@angular/router';
import { ProductService } from './../product.service';
import { Observable, Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit ,OnDestroy {

  categories=['Fruits', 'Dairy', 'Vegetables', 'Seasonings and Spices', 'Bread'];
  products:any; 
  filteredProducts:any=[];
  category: any=null;
  subscription: Subscription = new Subscription;
  constructor( private productService :ProductService , private route:ActivatedRoute) { 
  }
  
  ngOnInit(): void {
   this.subscription=this.productService.getAll().
   subscribe( products=> this.filteredProducts=this.products=products );
  


  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
catnull(){
  this.category=null;
  this.filteredProducts=this.products;
}
 
async filterByCategory(){
  await this.route.queryParamMap.subscribe(paramMap => {
    this.category = paramMap.get('category'); 
   
});
  this.filteredProducts=(this.category)? 
 this.products.filter( (p: { category: any; })=> p.category===this.category): this.filteredProducts;

}

  filter(srch:string){
    
    this.filteredProducts= (srch)?
    this.products.filter((p: { title: string; }) => p.title.toLowerCase().includes(srch.toLowerCase())):
    this.products;
    }

}
