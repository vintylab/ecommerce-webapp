import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterState, RouterStateSnapshot } from '@angular/router';
import { map } from 'rxjs/operators';
//import 'rxjs/add/operator/map';
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{

  constructor(private auth:AuthService, private router:Router) { }
  canActivate( route:any , state: RouterStateSnapshot)  {
if(this.auth.isloggedin)
return true;
this.router.navigate(['/login'], {queryParams:{returnUrl: state.url}});
return false;
  }



}
