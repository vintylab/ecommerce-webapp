import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from './../product.service';
import { FormsModule } from '@angular/forms';
import { AppCategory } from './../models/app-category';
import { map } from 'rxjs/operators';

import { Component, OnInit, Directive, OnDestroy } from '@angular/core';
import { AngularFireList } from '@angular/fire/database';
import { Observable, Subscription } from 'rxjs';
import {take} from 'rxjs/operators';
import { Product } from '../models/Product';
import { textChangeRangeIsUnchanged } from 'typescript';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnDestroy  {
   items=['Fruits','Dairy','Seasonings and Spices','Vegetables','Bread'];
   
   sub: Subscription = new Subscription;
  constructor(private router:Router , 
    private productService :ProductService) { }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }


    

 
save(product: Product){
  
 this.sub= this.productService.create(product).subscribe(p => console.log(p));
      
  this.router.navigate(['/admin/products']);
}




}
