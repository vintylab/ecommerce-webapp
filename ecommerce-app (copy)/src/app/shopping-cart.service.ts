import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Product } from './models/Product';
import {take} from 'rxjs/operators';
import { isTemplateSpan } from 'typescript';
import * as firebase from 'firebase';
import { HttpHeaders, HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  private url_quantity="http://localhost:8080/vintysite/cart/getQuantity/";
  private url_cart="http://localhost:8080/vintysite/cart/";
  private url_delete="http://localhost:8080/vintysite/cart/remove/";
  private hdrs=new HttpHeaders({
    'Content-Type': 'application/json',
    'response-Type':'text'
  });
  constructor(private http:HttpClient) { }

  addToCart(userId:any , productId:any){
    let options={
      headers:this.hdrs
    }
   return  this.http.post(this.url_cart+userId+'/add/'+ productId, options);


  }


removeFromCart(userId: any, productId: any){
  let options={
    headers:this.hdrs
  }
return this.http.post(this.url_cart+userId+'/remove/'+productId,options );

}

clearCart(userId:any){
  return this.http.delete(this.url_delete+userId);
}

quantity(userId:any,productId:any){
  let options={
    headers:this.hdrs
  }
return this.http.get(this.url_quantity+userId+'/'+productId,options);

}

getCart(userId:any){
  let options={
    headers:this.hdrs
  }
return this.http.get(this.url_cart+userId+'/getCart',options);

}


}
