import { ShoppingCartService } from './../shopping-cart.service';
import { map, switchMap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

import { Observable, Subscription } from 'rxjs';
import { ProductService } from './../product.service';
import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Product } from '../models/Product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent  implements OnInit, OnDestroy {
  
  
  productId!: string | null;
  category: string|null=null;
   sub1:Subscription=new Subscription;
   sub2:Subscription = new Subscription;
   sub3:Subscription = new Subscription;
   sub4:Subscription = new Subscription;
   sub5:Subscription = new Subscription;
product={title:"" , price:"" , category: "",imageUrl:"",description:"", productId:"" };
qua:string="0";
  constructor( private route: ActivatedRoute   ,
  private  productService: ProductService, 
    
   private cartService : ShoppingCartService) { 



}
  ngOnDestroy(): void {
   
   
  }
   ngOnInit(): void {
   this.route.paramMap.subscribe(param=> {
      this.productId=param.get('productId');
   // console.log(this.productId);
    this.productService.getById(this.productId!).
    subscribe(p => { this.product=p; 
      if(localStorage.getItem("userId"))
     
       this.cartService.quantity(localStorage.getItem("userId"),this.product.productId)
       .subscribe( p => {
         
         this.qua=p.toString() ; 
      } );
    
    
    } )
  });

  

  }
 

 
 


   private getQuantity(){
    if(localStorage.getItem('userId'))
   this.cartService.quantity(localStorage.getItem('userId'), this.product.productId)
   .subscribe( ob => this.qua =ob.toString());
   
   return this.qua;
  }


  addToCart(){
    this.cartService.addToCart(localStorage.getItem("userId"),this.product.productId)
   .subscribe(p=> {console.log(p);
    this.getQuantity();
  });


  }
    
  removeFromCart(){
    this.cartService.removeFromCart(localStorage.getItem("userId"), this.product.productId)
   .subscribe(p=>{console.log(p); this.getQuantity();});
   
  }
  
   
  
  



}
