import { Product } from './../models/Product';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from './../product.service';
import { FormsModule } from '@angular/forms';

import { map } from 'rxjs/operators';

import { Component, OnInit, Directive, OnDestroy } from '@angular/core';
import { AngularFireList } from '@angular/fire/database';
import { Observable, Subscription } from 'rxjs';
import {take} from 'rxjs/operators';

@Component({
  selector: 'product-form-update',
  templateUrl: './product-form-update.component.html',
  styleUrls: ['./product-form-update.component.css']
})
export class ProductFormUpdateComponent implements OnInit, OnDestroy {
   sub: Subscription = new Subscription;
   sub2: Subscription = new Subscription;
   sub3: Subscription = new Subscription;
  items=['Fruits','Dairy','Seasonings and Spices','Vegetables','Bread'];
   id: any;
   product={title:"" , price:"" , category: "",imageUrl:"",description:"" };
  constructor(private router:Router , 
    private productService :ProductService,
    private route: ActivatedRoute) { 

   }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
    this.sub2.unsubscribe();
    this.sub3.unsubscribe();
  }
  ngOnInit(): void {
    
this.id=this.route.snapshot.paramMap.get('id');
console.log(this.id);
if(this.id)this.sub= this.productService.getById(this.id).
subscribe(p => { this.product=p;console.log(p)});
  }

 
save(product: any){
  if(this.id) 
 this.sub2= this.productService.update(this.id,product ).subscribe(p=> console.log(p));
  console.log("save method called :" + product );
  this.router.navigate(['/admin/products']);
}

delete(){
  if(!confirm('Are you sure you want to delete this product?') )
  return ;
 this.sub3=this.productService.delete(this.id).subscribe(p =>console.log(p));
  this.router.navigate(['/admin/products']);
}

/*get productPrice() { return (this.product && this.product.price) ? this.product.price : null }
get productTitle() { return (this.product && this.product.title) ? this.product.title : null }
get productDescription() { return (this.product && this.product.description) ? this.product.description : null }
get productImageUrl() { return (this.product && this.product.price) ? this.product.price : null }
get productCategory(){return this.product && this.product.category ? this.product.category :null}*/

}
