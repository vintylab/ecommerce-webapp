import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Order } from './models/Order';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private url_save="http://localhost:8080/vintysite/order/createOrder";
  private url_all="http://localhost:8080/vintysite/order/getOrders";
  private url_my="http://localhost:8080/vintysite/order/";
  private hdrs=new HttpHeaders({
    'Content-Type': 'application/json',
    'response-Type':'text'
  });
  constructor(private http: HttpClient) { }

placeOrder(order:Order){
  let options={
    headers: this.hdrs
  }
return this.http.post(this.url_save,JSON.stringify(order),options);

}

getAll(){
  let options={
    headers: this.hdrs
  }
  return this.http.get(this.url_all,options);
}

getMy(){
  let options={
    headers: this.hdrs
  }
  let str=localStorage.getItem("userId");
  return this.http.get(this.url_my+str+'/getOrders',options);
}

}
