import { Router } from '@angular/router';
import { Product } from './../models/Product';
import { ProductService } from './../product.service';
import { ShoppingCartService } from './../shopping-cart.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
cart:any;
initialized=false;
  products: Product[] = [];
  constructor(private cartService:ShoppingCartService,
    private productService:ProductService,
    private router:Router
    ) { }

  ngOnInit(): void {

    this.cartService.getCart(localStorage.getItem("userId")!).subscribe( obj => {this.cart=obj;
      this.loadProducts();
      this.initialized=true;
    } );

    


  }
 private isloggedin(){
    if(localStorage.getItem("userId"))
    return true;
    return false;
   }

  getQuantity(){
    if(!this.isloggedin())
    return 0;
    let q=0;
    if(this.cart)
    this.cart.forEach((element: any) => {
      q+=element.quantity;
    });
  return q;
  }

  clearCart(){
if(!this.isloggedin())
return ;
this.cartService.clearCart(localStorage.getItem("userId")).subscribe(cl =>{console.log(cl);
  this.cartService.getCart(localStorage.getItem("userId")!).subscribe( obj => {this.cart=obj;
    this.products=[]; 
    this.loadProducts();

  } );
});

  }

  loadProducts(){
    if(this.cart)
    this.cart.forEach((element:any) => {
     this.productService.getById(element.productId.toString()).subscribe(obj => this.products.push(obj));

    });
    
  }

  myfn(){
  if   (typeof this.products !== 'undefined' && typeof this.products !== 'undefined'  )
  return true;
  return false;
  }

calculateTotal(){
let i=0;
let total=0;
if(this.myfn())
  this.products.forEach( (element) => { 
  total+=this.cart[i].quantity * element.price;
   i=i+1;
   });
   return total;
}

checkout(){

  this.router.navigate(['/checkout']);
}


}
