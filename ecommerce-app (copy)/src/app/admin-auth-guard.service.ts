import { AppUser } from './models/app-user';
import { AngularFireObject } from '@angular/fire/database';
import { UserService } from './user.service';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRoute, RouterStateSnapshot, Router } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import * as firebase from 'firebase';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminAuthGuardService implements CanActivate {
 isuserAdmin!: boolean;
  constructor(private auth: AuthService) { }

  canActivate ( route:any,state:RouterStateSnapshot ) {
    if(localStorage.getItem("userId")==="1")
   return true;
   else return false;
}

}