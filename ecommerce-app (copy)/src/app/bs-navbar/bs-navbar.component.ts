import { map } from 'rxjs/operators';
import { ShoppingCartService } from './../shopping-cart.service';
import { User } from './../models/User';
import { UserService } from './../user.service';

import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';


import { AuthService } from '../auth.service';
import { AdminAuthGuardService } from '../admin-auth-guard.service';
import { AppUser } from '../models/app-user';
import { Observable, Subscription } from 'rxjs';
@Component({
  selector: 'bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.css']
})
export class BsNavbarComponent implements OnInit , OnDestroy {
  

 user:any;
cart:any;
  sub1: Subscription = new Subscription;
  sub2: Subscription = new Subscription;
 
  constructor(public  auth : AuthService , 
    private userService:UserService,
    private cartService:ShoppingCartService,
    private ch : ChangeDetectorRef
    ) {

  }
  ngOnDestroy(): void {
    
  }
    ngOnInit(): void {
    if(localStorage.getItem("userId"))
    this.userService.getName(localStorage.getItem("userId")!).subscribe( p =>{
     this.user=p;
    
   });
  
   this.cartService.getCart(localStorage.getItem("userId")!).subscribe( obj => this.cart=obj );
   
  }
  logout(){
    
    this.userService.logout();

  }

  isloggedin(){
   if(localStorage.getItem("userId"))
   return true;
   return false;
  }

  uname(){
    
if(this.user)
return this.user.uname;
else return " ";
  }

 getQuantity(){
   if(!this.isloggedin())
   return 0;
   let q=0;
   if(this.cart)
   this.cart.forEach((element: any) => {
     q+=element.quantity;
   });
 return q;
 }



}
