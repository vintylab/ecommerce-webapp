import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { User } from '../models/User';
import { UserService } from '../user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private userService: UserService ,private router:Router) { }

  ngOnInit(): void {
  }


  signup(user: User){
    this.userService.signup(user);
  }

}
