import { UserService } from './user.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { observable, Observable, of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AppUser } from './models/app-user';
import {  switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isloggedin =true;
  users! : Observable<any> ;
  username!:string;
  constructor( private userService:UserService,
     private route: ActivatedRoute,private rout: Router) { 
      
  }
  login(){
    
    
  }
  
  logout(){
    localStorage.clear();
    this.isloggedin=false;
    location.reload();
  }

  
}
