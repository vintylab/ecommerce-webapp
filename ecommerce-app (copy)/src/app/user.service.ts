import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Injectable } from '@angular/core'  ;

import { AppUser } from './models/app-user' ;
import { User } from './models/User';
//import { FirebaseObjectObservable } from '@angular/fire/database' 

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url_signup="http://localhost:8080/vintysite/signup";
  private url_name="http://localhost:8080/vintysite/getName/";
  private url_login="http://localhost:8080/vintysite/login";
  
 
  private hdrs=new HttpHeaders({
    'Content-Type': 'application/json',
    'response-Type':'text'
  });
  user:any={};

  userId="0";
  constructor(private http:HttpClient, private route:ActivatedRoute,private router:Router) { 
  if(this.userId==="0"){}
  else {
  this.getName(this.userId).subscribe(p =>this.user=p );

  }


  }
   
signup(user:User){
  let options = {
    headers: this.hdrs
 }
return this.http.post(this.url_signup,JSON.stringify(user),options).subscribe(p=> {
   
  localStorage.setItem("userId",p.toString());
  this.user=user;
  console.log(this.user) ;
  this.user.password="";
  
  this.userId=p.toString();
  console.log(this.user) ;
  this.router.navigate(['/']);
  });

}

getName(userId: string){
  let options = {
    headers: this.hdrs
 }
return this.http.get(this.url_name+userId,options);

}


login(user: User){
  let returnUrl=this.route.snapshot.queryParamMap.get('returnUrl') || '/';
  localStorage.setItem('returnUrl',returnUrl);
  let options = {
    headers: this.hdrs
 }
 return  this.http.post(this.url_login,user,options);
  
  
}

logout(){
  localStorage.clear();
  this.user.userId="0";
  this.user.uname="";
  this.user.email="";


  location.reload();
 }



}
