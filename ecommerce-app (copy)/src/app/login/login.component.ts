import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { UserService } from './../user.service';
import  auth  from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as firebase from 'firebase';
import { AuthService } from '../auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnDestroy{
userId:any;
user:any;
sub:Subscription=new Subscription;
  constructor(private auth: AuthService, private userService:UserService, private router:Router ) { }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  

  login(user:any){
   this.sub= this.userService.login(user).subscribe( us => {this.userId=us.toString(); 
      this.userService.getName(this.userId).subscribe(p=> {this.user=p; 
      if(this.userId==="0"){
        console.log("Invalid mail or password");
      }
      else {
        localStorage.setItem("userId",this.userId);
        this.router.navigate(['/']);
    
      }
    } );
      });
      
  }

}
