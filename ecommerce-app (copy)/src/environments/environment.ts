// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:
  {
    apiKey: "AIzaSyAb4Vbyc3nyYZahqp0vPD4sfY7Ox5aIgb0",
    authDomain: "myfirebaseproject-1aa1f.firebaseapp.com",
    databaseURL: "https://myfirebaseproject-1aa1f-default-rtdb.firebaseio.com",
    projectId: "myfirebaseproject-1aa1f",
    storageBucket: "myfirebaseproject-1aa1f.appspot.com",
    messagingSenderId: "247791877805",
    appId: "1:247791877805:web:4fde8b7623d70991361024",
    measurementId: "G-XWGLD92WZF"

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
