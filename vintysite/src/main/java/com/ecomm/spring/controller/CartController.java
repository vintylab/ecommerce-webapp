package com.ecomm.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecomm.spring.model.Cart;
import com.ecomm.spring.service.CartService;
@CrossOrigin("*")
@RestController
public class CartController {

	@Autowired
private CartService cartService;	
	
	
	@GetMapping("/cart/getQuantity/{userId}/{productId}")
	public ResponseEntity <Integer>  getQuantity(@PathVariable("userId") int userId ,
			@PathVariable("productId") int productId  ) {
		int quantity=cartService.getQuantity(userId, productId);
		return ResponseEntity.ok().body(quantity);
	}
	
	@GetMapping("/cart/{userId}/getCart")
	public ResponseEntity <List<Cart> >  getCart(@PathVariable("userId") int userId) {
		
		return ResponseEntity.ok().body(cartService.list(userId));
	}
	
	@PostMapping("/cart/{userId}/add/{productId}")
	public ResponseEntity<?> addToCart(@PathVariable("userId") int userId,
			@PathVariable("productId") int productId  ){
		cartService.add(userId, productId);
return ResponseEntity.ok().body("\"Product added to cart or quantity increased by one\"");
	}
	
	@PostMapping("/cart/{userId}/remove/{productId}")
	public ResponseEntity<?> removeFromCart(@PathVariable("userId") int userId,
			@PathVariable("productId") int productId ){
		
		
		cartService.remove(userId, productId);
				return ResponseEntity.ok().body("\"Product removed from cart or quantity decreased by one\"");
	}
	
	@DeleteMapping("/cart/remove/{userId}")
	public ResponseEntity<?> removeUserFromCart(@PathVariable("userId") int userId ){
		
		
		cartService.removeUser(userId);
				return ResponseEntity.ok().body("\"All user items removed from cart\"");
	}
	
	
	

	
}
