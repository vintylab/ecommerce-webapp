package com.ecomm.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ecomm.spring.service.OrderService;
import com.ecomm.spring.model.Order;
import java.util.List;
@CrossOrigin("*")
@RestController
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	
@GetMapping("/order/getOrders")	
public ResponseEntity<List<Order> > getAll(){

	return ResponseEntity.ok().body(this.orderService.getAll());
}	


@GetMapping("/order/{userId}/getOrders")
public ResponseEntity<List<Order> > getOrders(@PathVariable int userId){

	return ResponseEntity.ok().body(this.orderService.getUserOrders(userId));
}


@PostMapping("/order/createOrder")
public ResponseEntity<?> createOrder(@RequestBody Order order){
	this.orderService.placeOrder(order);
	return ResponseEntity.ok().body("\" Ordered sucessfully \"");
}

}
