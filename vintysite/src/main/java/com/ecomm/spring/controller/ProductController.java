package com.ecomm.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ecomm.spring.model.Product;
import com.ecomm.spring.service.ProductService;
@CrossOrigin("*")
@RestController
public class ProductController {

	@Autowired
	private ProductService productService;
	
	//Get all the books
	@GetMapping("/products/getAll")
	public ResponseEntity <List <Product> > list(){
		
		
		List <Product> li=productService.list();
		return ResponseEntity.ok().body(li);
	}
	@PostMapping("/products/addProduct")
   public ResponseEntity<?> save(@RequestBody Product product){
	   productService.save(product);
	   return ResponseEntity.ok().body("\"Product uploaded in database with id: \"");
	   
   }	
	@GetMapping("/products/getById/{id}")
	public ResponseEntity<Product> get(@PathVariable("id") int id){
	return ResponseEntity.ok().body(productService.get(id));	
	}
	
	@PutMapping("/products/update/{id}")
	public ResponseEntity<?> update( @PathVariable("id") int id , @RequestBody Product product) {
		productService.update(id, product);
		return ResponseEntity.ok().body("\"Product has been updated id processesd: \"");
	}
	
	@DeleteMapping("/products/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id){
		productService.delete(id);
		return ResponseEntity.ok().body("\"Product removed from store\"");
	}
	
}
