package com.ecomm.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ecomm.spring.model.User;
import com.ecomm.spring.service.UserService;
@CrossOrigin("*")
@RestController
public class UserController {
 
	@Autowired
	private UserService userService;
@PostMapping("/login")	
public   ResponseEntity<Integer> LoginUser( @RequestBody User user )	{
	
	return ResponseEntity.ok().body(userService.LoginUser(user));
	
}
	  
@PostMapping("/signup")
public   ResponseEntity<Integer> Signup( @RequestBody User user)	{
	int p=userService.addUser(user);
	return ResponseEntity.ok().body(p);
	
}

@GetMapping("/getName/{userId}")
public   ResponseEntity<User> getName( @PathVariable int userId)	{
	return ResponseEntity.ok().body(userService.getUname(userId) );
}

	
	
	
}
