package com.ecomm.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ecomm.dao.UserDAO;
import com.ecomm.spring.model.User;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDAO userDAO;
	
	@Override
	@Transactional
	public int LoginUser(User user) {
	
		return userDAO.LoginUser(user.getEmail(), user.getPassword());
	}

	@Override
	@Transactional
	public int addUser(User user) {
		// TODO Auto-generated method stub
		return userDAO.addUser(user);
	}

	@Override
	@Transactional
	public User getUname(int userId) {
		// TODO Auto-generated method stub
		return userDAO.getUname(userId);
	}

}
