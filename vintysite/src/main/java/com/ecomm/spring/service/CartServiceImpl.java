package com.ecomm.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ecomm.dao.CartDAO;
import com.ecomm.spring.model.Cart;

@Service
public class CartServiceImpl implements CartService{

	@Autowired
	private CartDAO cartDAO;
	@Override
	@Transactional
	public int getQuantity(int userId, int productId) {
	return	cartDAO.getQuantity(userId, productId);
	}

	@Override
	@Transactional
	public List<Cart> list(int userId) {
		
		return cartDAO.list(userId);
	}

	@Override
	@Transactional
	public void add(int userId, int productId) {
		cartDAO.add(userId, productId);
	}

	@Override
	@Transactional
	public void remove(int userId, int productId) {
		cartDAO.remove(userId, productId);
		
	}
	
	@Override
	@Transactional
	public void removeUser(int userId) {
		cartDAO.removeUser(userId);
	}

}
