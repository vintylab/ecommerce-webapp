package com.ecomm.spring.service;

import java.util.List;

import com.ecomm.spring.model.Product;

public interface ProductService  {

	int  save(Product product);

	   Product get(int id);

	   List<Product> list();

	   void update(int id, Product product);

	   void delete(int id);	
	
}
