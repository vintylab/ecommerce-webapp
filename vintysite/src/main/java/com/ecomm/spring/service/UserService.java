package com.ecomm.spring.service;

import com.ecomm.spring.model.User;

public interface UserService {

public int LoginUser(User user) ;    //login
public int  addUser(User user);  //signup
public User getUname(int userId);     //get name of the user

}
