package com.ecomm.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecomm.dao.OrderDAO;
import com.ecomm.spring.model.Order;
@Service
public class OrderServiceImpl implements OrderService {
@Autowired
private OrderDAO orderDAO;	
	
	@Override
	public List<Order> getAll() {
		
		return this.orderDAO.getAll();
	}

	@Override
	public List<Order> getUserOrders(int userId) {
		
		return this.orderDAO.getUserOrders(userId);
	}

	@Override
	public void placeOrder(Order order) {
		this.orderDAO.placeOrder(order);
		
	}

	@Override
	public void cancelOrder(int orderId) {
		// TODO Auto-generated method stub
		
	}

}
