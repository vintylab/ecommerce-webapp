package com.ecomm.spring.service;
import java.util.List;

import com.ecomm.spring.model.Order;
public interface OrderService {

public List <Order> getAll();	 //get all orders irespective of user
public List <Order> getUserOrders(int userId);	//get orders of a particular user
public void placeOrder(Order order);   //place new order
public void cancelOrder(int orderId);  //cancel order

}
