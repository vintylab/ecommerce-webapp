package com.ecomm.spring.service;

import java.util.List;

import com.ecomm.spring.model.Cart;



public interface CartService {

	

	   int getQuantity(int userId, int productId); // get quantity of a particular product

	   List<Cart> list(int userId);      // get all cart details of a particular user

	   void add(int userId, int productId);  // add items to cart of some user

	   void remove(int userId, int productId);	 // remove items from cart  for a user
	   
	   void removeUser(int userId);
	
     }
