package com.ecomm.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ecomm.dao.ProductDAO;
import com.ecomm.spring.model.Product;

@Service
public class ProductServiceimpl implements ProductService {

	@Autowired
	private ProductDAO productDAO; 
	
	@Override
	@Transactional
	public int save(Product product) {
		
		return productDAO.save(product);
	}

	@Override
	@Transactional
	public Product get(int id) {
	return	productDAO.get(id);
		
	}

	@Override
	@Transactional
	public List<Product> list() {
		
		return productDAO.list();
	}

	@Override
	@Transactional
	public void update( int id, Product product) {
		productDAO.update(id, product);
		
	}

	@Override
	@Transactional
	public void delete(int id) {
		productDAO.delete(id);
		
	}

	
  	
	
	
	
}
