package com.ecomm.spring.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity(name="Orders")
public class Order {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
private int orderId;
private int userId;
private int totalPrice;
private String dateOrdered;
private String address;
public int getOrderId() {
	return orderId;
}
public void setOrderId(int orderId) {
	this.orderId = orderId;
}
public int getUserId() {
	return userId;
}
public void setUserId(int userId) {
	this.userId = userId;
}
public int getTotalPrice() {
	return totalPrice;
}
public void setTotalPrice(int totalPrice) {
	this.totalPrice = totalPrice;
}
public String getDateOrdered() {
	return dateOrdered;
}
public void setDateOrdered(String dateOrdered) {
	this.dateOrdered = dateOrdered;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
	
}
