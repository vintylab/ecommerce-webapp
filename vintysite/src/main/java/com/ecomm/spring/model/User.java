package com.ecomm.spring.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="User")
public class User {

@Id	
@GeneratedValue(strategy= GenerationType.IDENTITY)
private int userId;
private String uname;
private String email;
private String password;
private Boolean isAdmin;
public int getUserId() {
	return userId;
}
public void setUserId(int userId) {
	this.userId = userId;
}
public String getUname() {
	return uname;
}
public void setUname(String uname) {
	this.uname = uname;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public Boolean getIsAdmin() {
	return isAdmin;
}
public void setIsAdmin(Boolean isAdmin) {
	this.isAdmin = isAdmin;
}

	
	
}
