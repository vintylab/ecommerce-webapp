package com.ecomm.dao;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ecomm.spring.model.Cart;

@Repository
public class CartDAOImpl implements CartDAO {

	@Autowired
	private SessionFactory sessionFactory ;
	
	@Override
	public int getQuantity(int userId, int productId) {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Cart> criteria  = builder.createQuery(Cart.class);
		Root<Cart> root=criteria.from(Cart.class);
		Predicate[] predicates = new Predicate[2];
		predicates[0] = builder.equal(root.get("userId"), userId);
		predicates[1]=  builder.equal(root.get("productId"), productId);
		criteria.select(root).where(predicates);
		Cart cartitem=null;
		try {
		 cartitem = session.createQuery(criteria).getSingleResult();
		}
		catch(Exception e) {
			System.out.println("getting quantity");
		}
		if(cartitem!=null)
		return cartitem.getQuantity();
		else return 0;
	}

	@Override
	public List<Cart> list(int userId) {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Cart> criteria  = builder.createQuery(Cart.class);
		Root<Cart> root=criteria.from(Cart.class);
		Predicate[] predicates = new Predicate[1];
		predicates[0] = builder.equal(root.get("userId"), userId);
		criteria.select(root).where(predicates);
		
		 List<Cart> cartitems = null ;
		 try {
		 cartitems= session.createQuery(criteria).getResultList();
		 }
		 catch(Exception e) { System.out.println("Cart seems to be empty");}
		return cartitems;
	}

	@Override
	public void add(int userId, int productId) {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Cart> criteria  = builder.createQuery(Cart.class);
		Root<Cart> root=criteria.from(Cart.class);
		Predicate[] predicates = new Predicate[2];
		predicates[0] =  builder.equal(root.get("userId"), userId);
		predicates[1] =  builder.equal(root.get("productId"), productId);
		criteria.select(root).where(predicates);
		Cart cartitem=null;
		try {
		 cartitem= session.createQuery(criteria).getSingleResult();
		}
		catch(Exception e) {
			System.out.println("Added to cart ");
		}
		int q=0;
		if(cartitem != null  ) {
		q=cartitem.getQuantity() + 1;
		cartitem.setQuantity(q);
		session.update(cartitem);
		}
		else {
			Cart item=new Cart(); item.setProductId(productId); item.setUserId(userId);
			item.setQuantity(1);
			session.save(item);
			
		}
		
	}

	@Override
	public void remove(int userId, int productId) {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Cart> criteria  = builder.createQuery(Cart.class);
		Root<Cart> root=criteria.from(Cart.class);
		Predicate[] predicates = new Predicate[2];
		predicates[0] =  builder.equal(root.get("userId"), userId);
		predicates[1] =  builder.equal(root.get("productId"), productId);
		criteria.select(root).where(predicates);
		
		Cart cartitem=null;
		try {
		cartitem= session.createQuery(criteria).getSingleResult();
		} catch(Exception e) { System.out.println("No such product to remove");}
		
		int q=0;
		if(cartitem != null) {
		q=cartitem.getQuantity();
		if(q>1) {
		cartitem.setQuantity(q-1);
		session.update(cartitem);
		}
		else session.delete(cartitem);
		}
		
		
	}

	@Override
	public void removeUser(int userId) {
		
		Session session = sessionFactory.getCurrentSession();
		
		
		Query query=session.createQuery("delete Cart  where userId= :userId ");
		query.setParameter("userId", userId);
		try {
		query.executeUpdate();
		} catch(Exception e) { System.out.println("No such product to remove");}
		
		
		
		
		
	}
	
	
	
	

}
