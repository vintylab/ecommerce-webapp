package com.ecomm.dao;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ecomm.spring.model.User;

@Repository
public class UserDAOImpl implements UserDAO{

	@Autowired
private SessionFactory sessionFactory;
	@Override
	public int LoginUser(String email, String password) {
		Session session=sessionFactory.getCurrentSession();
		CriteriaBuilder builder =session.getCriteriaBuilder();	
		CriteriaQuery<User> criteria =builder.createQuery(User.class);
		Root<User> root=criteria.from(User.class);
		Predicate[] predicates=new Predicate[2];
		predicates[0] =  builder.equal(root.get("email"), email);
		predicates[1] =  builder.equal(root.get("password"), password);
		criteria.select(root).where(predicates);
		
		try {
			User us= session.createQuery(criteria).getSingleResult();
			 System.out.println("User found");
			 return us.getUserId();
			}
			catch(Exception e) {
				System.out.println("No such user, please enter valid credentials");
			return 0;
			}
		
		
		
	
	}

	@Override
	public int addUser(User user) {
		
		
	Session session=sessionFactory.getCurrentSession();
	CriteriaBuilder builder =session.getCriteriaBuilder();	
	CriteriaQuery<User> criteria =builder.createQuery(User.class);
	Root<User> root=criteria.from(User.class);
	Predicate[] predicates=new Predicate[1];
	String email=user.getEmail();
	predicates[0] =  builder.equal(root.get("email"), email);
	criteria.select(root).where(predicates);
	
	try {
		 session.createQuery(criteria).getSingleResult();
		 System.out.println("User with this email address already exists");
		 return 0;
		}
		catch(Exception e) {
		session.save(user);
	//	User us= session.createQuery(criteria).getSingleResult();
		return user.getUserId();
		}
		
	}

	@Override
	public User getUname(int userId) {
		Session session=sessionFactory.getCurrentSession();
		CriteriaBuilder builder =session.getCriteriaBuilder();	
		CriteriaQuery<User> criteria =builder.createQuery(User.class);
		Root<User> root=criteria.from(User.class);
		Predicate[] predicates=new Predicate[1];
		predicates[0] =  builder.equal(root.get("userId"), userId);
		criteria.select(root).where(predicates);
		try {
		   User us= session.createQuery(criteria).getSingleResult();
		   return us;
		    }
		catch(Exception e) {
			return null;
		}
		
	}

}
