package com.ecomm.dao;

import com.ecomm.spring.model.User;

public interface UserDAO {

	public int LoginUser(String email, String password) ;    //login
	public int  addUser(User user);  //signup
	public User getUname(int userId);     //get name of the user
	
	
}
