package com.ecomm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ecomm.spring.model.Product;
@Repository
public class ProductDAOimpl implements ProductDAO{

	@Autowired
private SessionFactory sessionFactory; 
	
	@Override
	public int save(Product product) {
		sessionFactory.getCurrentSession().save(product);
		return product.getProductId();
	}

	@Override
	public Product get(int id) {
		return sessionFactory.getCurrentSession().get(Product.class, id);
	}

	@Override
	public List<Product> list() {
	List<Product> li	=sessionFactory.getCurrentSession().createQuery("from Product").list();
		return li;
	}

	@Override
	public void update(int id, Product product) {
		
		product.setProductId(id);
		sessionFactory.getCurrentSession().update(product);
		
	}

	@Override
	public void delete(int id) {
	Session sn=sessionFactory.getCurrentSession();
	Product product = sn.byId(Product.class).load(id);
	sn.delete(product);		
	
		
	}

}
