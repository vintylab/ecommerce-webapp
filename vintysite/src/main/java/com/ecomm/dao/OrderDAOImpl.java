package com.ecomm.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ecomm.spring.model.Cart;
import com.ecomm.spring.model.Order;
@Repository
public class OrderDAOImpl implements OrderDAO {

	@Autowired
private SessionFactory sessionFactory;	
	
	@Override
	@Transactional
	public List<Order> getAll() {
	List<Order> li=	sessionFactory.getCurrentSession().createQuery("from Orders").list();
	return li;
	}

	@Override
	@Transactional
	public List<Order> getUserOrders(int userId) {
		Session session=sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Order> criteria  = builder.createQuery(Order.class);
		Root<Order> root=criteria.from(Order.class);
		Predicate[] predicates = new Predicate[1];
		predicates[0] = builder.equal(root.get("userId"), userId);
		criteria.select(root).where(predicates);
		List<Order> orders = null ;
		 try {
		 orders= session.createQuery(criteria).getResultList();
		 }
		 catch(Exception e) { System.out.println("Seems there are no orders at all");}
		return orders;
		
		
	
	}

	@Override
	@Transactional
	public int placeOrder(Order order) {
		sessionFactory.getCurrentSession().save(order);
		return order.getOrderId();
	}

	@Override
	@Transactional
	public void cancelOrder(int orderId) {
		// TODO Auto-generated method stub
		
	}

}
