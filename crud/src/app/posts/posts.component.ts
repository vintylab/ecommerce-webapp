import { HttpClient, HttpResponse } from '@angular/common/http';


import { Component, Injectable, Input } from '@angular/core';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})

export class PostsComponent  {
posts:any ;
tmp:any;
private url='https://jsonplaceholder.typicode.com/posts/';
  constructor(private http : HttpClient) { 

    http.get(this.url)
    .subscribe(response  => {
      this.posts=response;
    });
  }
createPost(input:HTMLInputElement){
  let pos={title:input.value};
  input.value='';
  this.http.post(this.url,JSON.stringify(pos)).
  subscribe(response =>{
   this.tmp=response;
   
   pos['id']=this.tmp.id;
   this.posts.splice(0,0,pos);
  });
}  
updatePost(post){

this.http.patch(this.url +'/'+post.id ,JSON.stringify({isRead:true}) ).
subscribe(response =>{
console.log(response);
});

}
  
deletePost(post){
this.http.delete(this.url+'/'+post.id).
subscribe(response =>{
console.log(response);
let index =this.posts.indexOf(post);
this.posts.splice(index,1);

});


}

}
